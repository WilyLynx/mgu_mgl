# Metody Głębokiego Uczenia - projekt

Repozytorium zawiera wszystkie projekty realizowane w ramach przedmiotu Metody Głębokiego Uczenia.

Poszczególne projekty znajdują się w folderach z przedrostkiem project_ .

Kontrybutorzy:

- Damian Kryński
- Michał Możdżonek

Uczelnia: Politechnika Warszawska

Wydział: Matematyki i Nauk Informacyjnych

Kierunek studiów: Analiza i Inżynieria Danych - magisterskie
