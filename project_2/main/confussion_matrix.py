import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import utils
from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.layers import Dense, Activation, Flatten, Dropout, BatchNormalization
from tensorflow.keras.layers import Conv2D, MaxPooling2D, GlobalAveragePooling2D
from tensorflow.keras.datasets import cifar10
from tensorflow.keras import regularizers
from tensorflow.keras.callbacks import LearningRateScheduler
from tensorflow.keras.optimizers import RMSprop
import numpy as np
from matplotlib import pyplot as plt
from tensorflow.keras.utils import model_to_dot
from tensorflow.keras.applications.resnet50 import ResNet50
from tensorflow.keras import regularizers
import os
import json
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

(train_images, train_labels), (test_images, test_labels) = cifar10.load_data()
test_labels_as_list = test_labels.flatten()
real_labels = ['airplane', 'automobile', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck']

scale = True

if scale:
  train_images = train_images.astype('float32')
  test_images = test_images.astype('float32')

  # z-score
  mean = np.mean(train_images, axis=(0, 1, 2, 3))
  std = np.std(train_images, axis=(0, 1, 2, 3))
  train_images = (train_images - mean) / (std + 1e-7)
  test_images = (test_images - mean) / (std + 1e-7)


num_classes = 10
train_labels = utils.to_categorical(train_labels, num_classes)
test_labels = utils.to_categorical(test_labels, num_classes)

for dir in os.walk('../research'):
    if dir[0] != '../research':
        print(dir[0])
        path = dir[0]+'/model.json'

        print(os.path.isfile(path))
        with open(path) as f:
            model_schema = json.dumps(json.load(f))
            model = tf.keras.models.model_from_json(model_schema)

        weights_path = dir[0]+'/model.h5'
        if os.path.isfile(weights_path):
            model.load_weights(dir[0]+'/model.h5')

        model.compile(
            optimizer='adam',
            loss=tf.keras.losses.CategoricalCrossentropy(),
            metrics=['accuracy']
        )

        test_loss, test_acc = model.evaluate(test_images, test_labels, verbose=2)
        print('Test acc: '+str(test_acc))
        with open('{}/acc_{}.txt'.format(dir[0], str(test_acc)), 'w') as acc_file:
            acc_file.write(str(test_acc))

        predictions = model.predict(test_images)
        predictions = np.array([np.argmax(p) for p in predictions])

        confusion_matrix = tf.math.confusion_matrix(test_labels_as_list, predictions).numpy()
        conf_matrix_df = pd.DataFrame(confusion_matrix, columns=real_labels, index=real_labels)

        # latex format
        with open(dir[0]+'/conf_matrix.tex', 'w') as latex_file:
            conf_matrix_df.to_latex(latex_file)
        # html format
        with open(dir[0]+'/conf_matrix.html', 'w') as html_file:
            conf_matrix_df.to_html(html_file)

        # heatmap
        plt.clf()
        ax = sns.heatmap(conf_matrix_df, annot=True, square=True, fmt='d', cmap="YlGnBu")
        ax.invert_yaxis()
        plt.ylim(-0.5, 10)
        plt.xticks(rotation=55)
        plt.subplots_adjust(bottom=0.2)
        plt.savefig(dir[0]+'/conf_matrix.svg')


