# Projekt II - sieci konwolucyjne

Celem projektu było zbodowanie modelu potrafiącego klasyfikować dane z zbioru CIFAR-10.

## Struktura plików

Folder doc zawiera raport z przeprowadzenia projektu.

W folderze main możemy znaleźć **notebook**, który zawiera wszystkie modele przebadane przez nas zespół. Zespół w czasie projektu korzystał z serwisu Google Colab. Zapisany notebook także jest dostępny w tym serwisie. Link do notebooka: [Jupyter notebook](https://colab.research.google.com/drive/10R-tcDQyN3Izl5qqppj-g8GlCQ6HRNIh). Folder zawiera także skrypt tworzący macierze pomyłek dla modeli znajdujących się w folderze research.

Forlder research zawiera zapisane wytrenowane modele. Modele są ponumerowane według kolejności wystąpienia w raporcie oraz opisane imieniem. W folderze każdego modelu możemy znaleźć:

- model.h5 - plik z zapisanymi wagami sieci
- model.json - architektura sieci zapisana metodą model.to_json()
- arch.png - wizualizacja architektury
- accuracy.svg - wykres accuracy z procesu uczenia
- loss.svg - wykres funkcji błędu z procesu uczenia
- conf_matrix.[html, svg, tex] - macierz pomyłek w jednej z trzech formatów
- acc_ - plik z końcową skutecznością modelu
