# Kontrola postępów

Chcielibyśmy zaprezentować to co udało nam się zrobić do tej pory. Kod projektu dostępny w folderze main.

Przygotowaliśmy model sieci (main.model). Model zawiera listę warstw oraz metody:
 forward (obliczenie sieci), fit (wsteczna propagacja)
 
 W folderze main.model.functions zamieściliśmy testowane funkcję aktywacji:
 * linear
 * sigmoid
 * relu
 * leaky relu
 * swish
 * tanh
 * softmax
 
 Funkcję takie jak relu, leaky relu, swish stosuję się głównie w sieciach głębokich
 , ale postanowiliśmy przetestować ich działanie także w przypadku sieci płytkich
 
 Jako funkcję błędu używamy błędu średnio-kwadratowego. 
 
 ### Testy
 
 Przeprowadziliśmy podstawowe testy na przygotowanych zbiorach. Testy będziemy zamieszczać w folderze reports z sufiksem _paper.
 
 ##### Przykładowy wynik testu dla zbioru data.simple 
 
 ![Simple](simple_classification.png)
 
 Sieć składa się z jednej warstwy ukrytej oraz warstwy wynikowej z funkcją aktywacji softmax.
 Na podanym zbiorze nie widać różnicy, ale w warstwie ukrytej wykorzystane zostały różne funkcje aktywacji:
* linear
* sigmoid
* leaky_relu
* relu
* swish
* tanh

Oprócz tego w konsoli zostaną wypisane statystyki procesu uczenia każdego modelu.
![console](Screenshot from 2020-03-18 11-30-57.png)

 ##### Przykładowy wynik testu dla zbioru data.three_gauss
 ![Gauss](classification_three_gaus.png)
 
 Ten test różni się od pierwszego jedynie wykorzystanym zbiorem.
 
 
 ##### Przykładowy wynik testu regresji dla zbioru activation 100
 ![regresja](regression_plot.png)
 
 W tym przypadku wykorzystano ten sam zestaw funkcji aktywacji, zmieniono jednak funkcję aktywacji
 w warstwie wyjściowej na liniową.