import numpy as np


class dense:
    """
    Class representing single layer in NN
    """

    def __init__(
            self,
            in_len,
            out_len,
            activation,
            enable_bias
    ):
        self.in_len = in_len
        self.out_len = out_len
        self.enable_bias = enable_bias
        self.activation = activation
        self.weights = np.random.normal(0, 1, (out_len, in_len))  # weights init N(0,1)
        self.bias = np.vstack(np.zeros(out_len))  # bias init 0

    def forward(self, in_vector):
        output = self._mul_weights(in_vector)
        output = self._apply_bias(output)
        output = self.activation.func(output)
        return output

    def forward_with_internal_state(self, in_vector):
        """
        Returns vector value before applying activation function
        and output vector from layer
        """
        internal = self._mul_weights(in_vector)
        internal = self._apply_bias(internal)
        output = self.activation.func(internal)
        return internal, output

    def _mul_weights(self, in_vector):
        """
        Multiplies input vector by weights matrix
        in_vector should be vertical vector, shape (n,1)
        """
        return np.vstack(np.matmul(self.weights, in_vector))

    def _apply_bias(self, in_vector):
        """
        Checks if bias is enabled. YES -> adds bias to vector
        """
        if self.enable_bias:
            return in_vector + self.bias
        else:
            return in_vector

    def set_weigths(self, new_matrix):
        self.weights = new_matrix

    def __str__(self):
        layer_type = "Dense: "
        layer_size = "(input size: {}, output size: {})".format(self.in_len,self.out_len)
        activation_function = " function: " + str(self.activation)
        return layer_type + layer_size + activation_function