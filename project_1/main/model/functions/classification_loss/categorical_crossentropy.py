import numpy as np


class categorical_crossentropy:

    def loss(self, x, t, eps=1e-15):
        """
        Categorical crossentropy loss
        x - input vector
        t - target vector

        Return single loss value
        """
        clipped = np.clip(x, eps, 1-eps)
        return - np.sum(t*np.log(clipped))

    def derivative(self, x, t, eps=1e-15):
        """
        Categorical crossentropy loss derivative
        x - input vector
        t - target vector

        Returns derivative vector for each component in x
        """
        clipped = np.clip(x, eps, 1-eps)
        return - t / clipped
