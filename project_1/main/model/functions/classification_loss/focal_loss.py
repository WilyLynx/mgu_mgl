import numpy as np

class focal_loss:

    def __init__(self, gamma=1):
        self.gamma = gamma


    def loss(self, x, t, eps=1e-15):
        """
        Focal loss
        https://arxiv.org/pdf/1708.02002.pdf
        x - input vector
        t - target vector

        Return single loss value
        """
        clipped = np.clip(x, eps, 1-eps)
        return - np.sum(np.power((1-clipped), self.gamma)*t*np.log(clipped))

    def derivative(self, x, t, eps=1e-15):
        """
        Categorical crossentropy loss derivative
        x - input vector
        t - target vector

        Returns derivative vector for each component in x
        """
        clipped = np.clip(x, eps, 1-eps)
        return t * np.power((1-clipped), self.gamma) * (self.gamma * clipped * np.log(clipped) + clipped - 1)
