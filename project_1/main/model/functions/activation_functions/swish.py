from .function_helper import function_helper
from .linear import linear
from .sigmoid import sigmoid


class swish(function_helper):
    def __init__(self):
        self._sigmoid = sigmoid()
        self._linear = linear()
        self.func = lambda x: self._sigmoid.func(x) * self._linear.func(x)
        self.d_func = lambda x: self._sigmoid.d_func(x) * self._linear.func(x) \
                              + self._linear.d_func(x) * self._sigmoid.func(x)

    def __str__(self):
        return "swish"

