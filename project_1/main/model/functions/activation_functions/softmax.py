import numpy as np

from .function_helper import function_helper


class softmax(function_helper):
    def __init__(self):
        self.func = self._softmax
        self.d_func = self._softmax_derivative

    def _softmax(self, vec):
        ex = np.exp(vec)
        return ex / sum(ex)

    def _softmax_derivative(self, vec):
        vector_len = len(vec)
        derivative = np.zeros((vector_len, vector_len))
        func_val = self._softmax(vec)
        for i in range(vector_len):
            for j in range(vector_len):
                if i == j:
                    derivative[i][j] = func_val[i] * (1 - func_val[i])
                else:
                    derivative[i][j] = - func_val[i] * func_val[j]
        return derivative

    def __str__(self):
        return "softmax"
