from math import exp
import numpy as np


class function_helper:
    """
    Klasa reprezetująca parę funkcja (func), pierwsza pochodna funcji
    func - funkcja wektorowa R^n -> R^n
    d_func - funkcja obliczająca pochodną dla zadanego wektora.
        Pochodna reprezentowana w postaci Jakobianu
    """

    def __init__(self, func, d_func):
        self.func = func
        self.d_func = d_func

    def __str__(self):
        return ""
