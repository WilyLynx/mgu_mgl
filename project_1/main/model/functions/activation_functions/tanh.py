from .function_helper import function_helper
import numpy as np


class tanh(function_helper):
    def __init__(self):
        self.func = lambda x: np.tanh(x)
        self.d_func = lambda x: np.diagflat(1 - self.func(x) * self.func(x))

    def __str__(self):
        return "tanh"
