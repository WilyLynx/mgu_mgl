from .function_helper import function_helper
import numpy as np


class relu(function_helper):
    def __init__(self):
        self.func = lambda x: np.maximum(0.0*x, x)
        self.d_func = self._relu_derivaive

    def __str__(self):
        return "relu"


    def _relu_derivaive(self, vec):
        res = np.zeros(len(vec))
        for i in range(len(vec)):
            if vec[i] > 0:
                res[i] = 1.0
            else:
                res[i] = 0
        return np.diag(res)
