from .function_helper import function_helper
import numpy as np


class leaky_relu(function_helper):
    def __init__(self):
        self.func = lambda x: np.maximum(0.1*x, x)
        self.d_func = self._leaky_relu_derivative

    def __str__(self):
        return "leaky_relu"

    def _leaky_relu_derivative(self, vec):
        res = np.zeros(len(vec))
        for i in range(len(vec)):
            if vec[i] < 0:
                res[i] = 0.1
            else:
                res[i] = 1.0

        return np.diag(res)
