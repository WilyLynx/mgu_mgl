from .function_helper import function_helper
import numpy as np


class sigmoid(function_helper):
    def __init__(self):
        self.func = lambda x: 1 / (1 + np.exp(np.negative(x)))
        self.d_func = lambda x: np.diagflat((self.func(x) * (1 - self.func(x))))

    def __str__(self):
        return "sigmoid"