import numpy as np

from .function_helper import function_helper


class linear(function_helper):
    def __init__(self):
        self.func = lambda x: x
        self.d_func = lambda x: np.diag(np.ones(len(x)))

    def __str__(self):
        return "linear"
