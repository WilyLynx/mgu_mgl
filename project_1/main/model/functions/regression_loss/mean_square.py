class mean_square:

    def loss(self, x, t):
        """
        Quadratic loss
        x - input vector
        t - target vector

        Return single loss value
        """
        diff = x - t
        return sum(diff * diff) / len(x)

    def derivative(self, x, t):
        """
        Quadratic loss derivative
        x - input vector
        t - target vector

        Returns derivative vector for each component in x
        """
        return 2 * (x - t) / len(x)

    def __str__(self):
        return "quadratic loss"
