import numpy as np


class huber_loss:
    def __init__(self, delta=500):
        self.delta = delta

    def loss(self, x, t):
        """
        Huber loss, Smooth Mean Absolute Error
        x - input vector
        t - target vector

        Return single loss value
        """
        abs_diff = np.abs(t - x)
        res = []
        for v in abs_diff:
            if v > self.delta:
                res.append(self.delta * v - self.delta * self.delta / len(x))
            else:
                res.append(v * v / len(x))
        return np.sum(res)


    def derivative(self, x, t):
        """
        Huber loss derivative,
        Smooth Mean Absolute Error
        x - input vector
        t - target vector

        Returns derivative vector for each component in x
        """
        abs_diff = np.abs(t - x)
        res = []
        for i in range(len(abs_diff)):
            v = abs_diff[i]
            if v > self.delta:
                res.append(self.delta * np.sign(x[i]))
            else:
                res.append(2*(x[i]-t[i])/len(x))
        return np.vstack(res)

