import numpy as np


def table_matrix_sum_col(table):
    sum_correction = []
    for m in table[0]:
        sum_correction.append(np.zeros(
            shape=m.shape
        ))

    for i in range(len(table)):
        for j in range(len(table[i])):
            sum_correction[j] += table[i][j]

    return sum_correction


class optimizer:

    def __init__(self):
        self.last_W_correction = None
        self.last_B_correction = None

    def get_delta_Last(self, expected_result, internal_state, loss_fun, activation):
        output_layer_z, output_layer_a = internal_state[-1]
        d_error = loss_fun.derivative(output_layer_a, expected_result)
        return np.matmul(activation.d_func(output_layer_z), d_error)

    def get_delta_l(self, next_layer_weights, last_delta_l, z_l, activation):
        return np.transpose(np.matmul(
            np.transpose(np.matmul(np.transpose(next_layer_weights), last_delta_l)),
            activation.d_func(z_l)))

    def calculate_corrections(self, model, data_input, internal_state, loss_fun, expected_result):
        last_layer_activation = model.layers_list[-1].activation
        delta_L = self.get_delta_Last(expected_result, internal_state, loss_fun, last_layer_activation)
        if len(model.layers_list) > 1:
            last_layer_weights_correction = np.matmul(internal_state[-2][1], np.transpose(delta_L))
        else:
            last_layer_weights_correction = np.matmul(data_input, np.transpose(delta_L))

        bias_correction = [delta_L]
        weights_correction = [last_layer_weights_correction]

        last_delta_l = delta_L
        if len(model.layers_list) > 1:
            for i in range(len(model.layers_list) - 2, -1, -1):
                delta_l = self.get_delta_l(
                    model.layers_list[i + 1].weights,  # next layer weights
                    last_delta_l,  # delta from next layer
                    internal_state[i][0],  # z value from current layer
                    model.layers_list[i].activation  # current layer activation
                )
                bias_correction.append(delta_l)
                if i > 0:
                    a_l_minus_1 = internal_state[i - 1][1]  # output value from prev layer
                else:
                    a_l_minus_1 = data_input  # model input vector

                weights_correction.append(
                    np.matmul(a_l_minus_1, np.transpose(delta_l))
                )
                last_delta_l = delta_l

        return weights_correction[::-1], bias_correction[::-1]

    def get_sum_correction(self, weights_correction, bias_correction):
        W_sum_correction = table_matrix_sum_col(weights_correction)
        b_sum_correction = table_matrix_sum_col(bias_correction)
        return W_sum_correction, b_sum_correction

    def apply_correction(self, model, weights_correction, bias_correction, eta, alpha=0.0):
        for w_correction, b_correction, model_l in \
                zip(weights_correction, bias_correction, model.layers_list):
            model_l.weights -= eta * np.transpose(w_correction)
            model_l.bias -= eta * b_correction

        if self.last_W_correction and self.last_B_correction and alpha > 0.0:
            for model_l, prev_w_correction, prev_b_correction in \
                    zip(model.layers_list, self.last_W_correction, self.last_B_correction):
                model_l.weights += alpha * np.transpose(prev_w_correction)
                model_l.bias += alpha * prev_b_correction

        self.last_W_correction = weights_correction
        self.last_B_correction = bias_correction

    def __str__(self):
        return "optimizer"
