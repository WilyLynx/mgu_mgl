import numpy as np


class validator:
    def __init__(self, model):
        self.model = model

    def get_loss(self, validation):
        good_predictions = 0
        validation_len = 0
        for data, label_vec in validation:
            label = np.argmax(label_vec)
            prediction = np.argmax(self.model.forward([data]))
            if label == prediction:
                good_predictions += 1
            validation_len += 1

        return 1 - good_predictions / validation_len

    def get_error(self, validation, loss_fun):
        data, label_vec = validation
        label_vec = np.array(label_vec).flatten()
        predictions = np.array(self.model.forward(data)).flatten()
        return loss_fun.loss(label_vec, predictions)

    def __str__(self):
        return "validator"
