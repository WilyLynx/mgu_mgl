import numpy as np


class Model:
    def __init__(
            self,
            layers_list,
            name=""
    ):
        self.name = name
        self.layers_list = layers_list

    def forward(self, input_batch):
        """
        Calculate network result for given input_batch.
        input_batch: list of vectors 1 x n where n is in_len of first layer
        Output: list of network output from last layer - one per input vector
        """
        result = []
        for data in input_batch:
            data = np.vstack(data)
            for l in self.layers_list:
                data = l.forward(data)
            # add network output to result list
            result.append(data)
        return result

    def fit(self, input_batch, loss_function, eta, optimizer):
        """
        Train network on given input_batch
        """
        weights_correction = []
        bias_correction = []
        for input_data, expected_class in input_batch:
            expected_result = np.vstack(expected_class)
            input_data = np.vstack(input_data)
            data = input_data
            # forward data
            internal_state_tape = []
            for layer in self.layers_list:
                res = layer.forward_with_internal_state(data)
                internal_state_tape.append(res)
                data = res[1]

            last_layer_z, last_layer_a = internal_state_tape[-1]
            error = loss_function.loss(last_layer_a, expected_result)

            w_c, b_c = optimizer.calculate_corrections(self,
                                                       input_data,
                                                       internal_state_tape,
                                                       loss_function,
                                                       expected_result)
            weights_correction.append(w_c)
            bias_correction.append(b_c)

        w_sum, b_sum = optimizer.get_sum_correction(weights_correction, bias_correction)
        optimizer.apply_correction(self, w_sum, b_sum, eta)

    def print(self):
        for l in self.layers_list:
            print(l.weights)
            print(l.bias)
            print("\n")

    def __str__(self):
        model_name = " MODEL: " + self.name + " "
        first_line = 20 * "=" + model_name + 20 * "="
        middle_lines = ""
        for layer in self.layers_list:
            middle_lines = middle_lines + str(layer) + "\n"
        last_line = (40 + len(model_name)) * "="
        return "{}\n{}{}".format(first_line, middle_lines, last_line)

    def get_out_len(self):
        return self.layers_list[-1].out_len
