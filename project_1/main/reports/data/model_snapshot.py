class ModelSnapshot:
    def __init__(self, model, fit_parameters, validation_parameters, data_name, statistics):
        self.model = str(model)
        self.fit_parameters = fit_parameters
        self.validation_parameters = validation_parameters
        self.data_name = data_name
        self.statistics = statistics
        self.statistics['best_train_indices'] = self.statistics['best_train_indices'][0]
        self.statistics['best_validation_indices'] = self.statistics['best_validation_indices'][0]

    def __str__(self):
        line = 150 * "*"
        snapshot_title = "MODEL SNAPSHOT:\n"
        model_str = str(self.model)
        fit_parameters_title = "FIT PARAMETERS:\n"
        fit_parameters = str_from_dictionary(self.fit_parameters)
        validation_parameters_title = "VALIDATION PARAMETERS:\n"
        validation_parameters = str_from_dictionary(self.validation_parameters)
        data_set_name_title = "TRAINED ON DATASET:\n"
        data_set_name = self.data_name
        statistics_title = "STATISTICS:\n"
        statistics = str_from_dictionary(self.statistics)
        return "{}\n{}{}\n{}{}\n\n{}{}\n{}{}\n{}{}{}\n".format(line,
                                                               snapshot_title,
                                                               model_str,
                                                               data_set_name_title,
                                                               data_set_name,
                                                               fit_parameters_title,
                                                               fit_parameters,
                                                               validation_parameters_title,
                                                               validation_parameters,
                                                               statistics_title,
                                                               statistics,
                                                               line)


def str_from_dictionary(dictionary):
    """
    Make a pprint string from dictionary
    """
    string = ""
    for item in dictionary:
        item_string = "{:35}{}\n".format(str(item) + ":", str(dictionary[item]))
        string = string + item_string
    return string
