import main.reports.data.abstract_data_loader as dl
import pandas as pd
import numpy as np


class Regression_loader(dl.Abstract_data_loader):
    default_label_column = 1

    def __init__(self, parent_folder):
        super().__init__(parent_folder)
        self.label_column = 1

    def get_default_path(self):
        return self.get_default_data_path_for_problem("regression")

    def load_data(self, path, label_column, split_ratio=0.7, shuffle=False, header=0):
        raw = pd.read_csv(path, header=header, sep=',').to_numpy()
        if shuffle:
            np.random.shuffle(raw)
        raw_len = len(raw)
        data_columns = list(range(raw.shape[1]))
        data_columns.pop(label_column)
        data = raw[:, data_columns]
        labels = raw[:, label_column]

        split_index = int(split_ratio * raw_len)
        train = data[:split_index, ]
        train_labels = labels[:split_index]
        test = data[split_index:, ]
        test_labels = labels[split_index:]

        return (train, train_labels), (test, test_labels)

    def load_test_data(self, path, label_column, header=1):
        classification_data_sets_paths = {x.name: x for x in self.default_path.iterdir() if x.is_file()}
        raw = pd.read_csv(classification_data_sets_paths[path], header=header, sep=',').to_numpy()

        raw_len = len(raw)
        data_columns = list(range(raw.shape[1]))
        data_columns.pop(label_column)
        data = raw[:, data_columns]
        labels = raw[:, label_column]

        return data, labels

    def get_labels(self, train_labels, test_labels):
        return [[l] for l in train_labels], [[l] for l in test_labels]

    def normalize_labels(self, labels, l_min, l_max):

        for i in range(len(labels)):
            labels[i] = 2*(labels[i]-l_min)/ (l_max-l_min) - 1
        return labels
