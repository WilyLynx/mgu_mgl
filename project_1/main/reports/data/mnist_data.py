from .classification_data import Classification_data_loader
import pandas as pd


class Mnist_data_loader(Classification_data_loader):
    def __init__(self, parent_folder, head=0):
        super().__init__(parent_folder)
        self.label_column = 0
        self.head = head

    def get_default_path(self):
        return self.get_default_data_path_for_problem("digit-recognizer")

    def load_data(self, path, label_column, split_ratio=0.7, shuffle=False, header=0):
        (train, train_labels), (test, test_labels) = \
            super(Mnist_data_loader, self).load_data(path, label_column, split_ratio, shuffle, header)
        if self.head > 0:
            train = train[0:self.head]
            train_labels = train_labels[0:self.head]
            test = test[0:self.head]
            test_labels = test_labels[0:self.head]
        return (train / 255.0, train_labels), (test / 255.0, test_labels)

    def load_test_data(self, path):
        classification_data_sets_paths = {x.name: x for x in self.default_path.iterdir() if x.is_file()}
        raw = pd.read_csv(classification_data_sets_paths[path], header=0, sep=',').to_numpy()
        return raw / 255.0

