from pathlib import Path
import pandas as pd
import numpy as np
from abc import abstractmethod


class Abstract_data_loader:
    default_split_ratio = 0.8
    default_shuffle = True

    def __init__(self, parent_folder):
        self.label_column = 0
        self.parent_folder = parent_folder
        self.default_path = self.get_default_path()

    @abstractmethod
    def get_default_path(self):
        pass

    @abstractmethod
    def get_labels(self, train_labels, test_labels):
        pass

    def get_default_data_path_for_problem(self, problem_type):
        path = Path().resolve()
        while path.name != self.parent_folder:
            path = path.joinpath('..').resolve()
        path = path.joinpath("projekt1").joinpath(problem_type)
        return path.resolve()

    def load_data(self, path, label_column, split_ratio=0.7, shuffle=False, header=0):
        raw = pd.read_csv(path, header=header, sep=',').to_numpy()
        if shuffle:
            np.random.shuffle(raw)
        raw_len = len(raw)
        data_columns = list(range(raw.shape[1]))
        data_columns.pop(label_column)
        data = raw[:, data_columns]
        labels = raw[:, label_column]
        labels_mapping = {k: v for v, k in enumerate(sorted(set(labels)))}
        converted_labels = []
        for l in labels:
            converted_labels.append(labels_mapping[l])

        split_index = int(split_ratio * raw_len)
        train = data[:split_index, ]
        train_labels = converted_labels[:split_index]
        test = data[split_index:, ]
        test_labels = converted_labels[split_index:]

        return (train, train_labels), (test, test_labels)

    def get_data_set(self,
                     filename,
                     split_ratio=default_split_ratio,
                     shuffle=default_shuffle):
        classification_data_sets_paths = {x.name: x for x in self.default_path.iterdir() if x.is_file()}
        return self.load_data(path=classification_data_sets_paths[filename],
                              label_column=self.label_column,
                              split_ratio=split_ratio,
                              shuffle=shuffle)

    def list_possible_data_sets(self):
        classification_data_sets_paths = {x.name: x for x in self.default_path.iterdir() if x.is_file()
                                          and x.name.endswith('.csv')}
        return [e for e in classification_data_sets_paths]

    def get_all_data_sets(self,
                          split_ratio=default_split_ratio,
                          shuffle=default_shuffle):
        return {(x.name, self.load_data(path=x,
                                        label_column=self.label_column,
                                        split_ratio=split_ratio,
                                        shuffle=shuffle)) for x in self.default_path.iterdir() if x.is_file()}
