from pathlib import Path
import numpy as np
import pandas as pd

import main.reports.data.abstract_data_loader as dl


class Classification_data_loader(dl.Abstract_data_loader):

    def __init__(self, parent_folder):
        super().__init__(parent_folder)
        self.label_column = 2

    def get_default_path(self):
        return self.get_default_data_path_for_problem("classification")

    def get_labels(self, train_labels, test_labels):
        labels = train_labels + test_labels
        unique = len(set(labels))
        train_class_l = []
        test_class_l = []
        for label in train_labels:
            class_l = self.make_classification_label(label, unique)
            train_class_l.append(class_l)
        for label in test_labels:
            class_l = self.make_classification_label(label, unique)
            test_class_l.append(class_l)

        return train_class_l, test_class_l

    def load_test_data(self, path, label_column, header=1):
        classification_data_sets_paths = {x.name: x for x in self.default_path.iterdir() if x.is_file()}
        raw = pd.read_csv(classification_data_sets_paths[path], header=header, sep=',').to_numpy()
        raw_len = len(raw)
        data_columns = list(range(raw.shape[1]))
        data_columns.pop(label_column)
        data = raw[:, data_columns]
        labels = raw[:, label_column]

        labels_mapping = {k: v for v, k in enumerate(sorted(set(labels)))}
        converted_labels = []
        for l in labels:
            converted_labels.append(labels_mapping[l])

        return data, converted_labels

    def make_classification_label(self, label, unique):
        class_l = np.zeros(unique)
        class_l[int(label)] = 1.0
        return class_l
