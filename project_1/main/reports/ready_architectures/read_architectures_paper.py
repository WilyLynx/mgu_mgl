import pathlib

# =============================== CONFIG ====================================================================
from main.reports.utils.model_reader import ModelReader

saved_models_path = pathlib.Path.cwd().joinpath("saved_models_architectures")
verbose_available_models_names = True

# available models -----------------------------------------------------------------------------------------
available_models = {x.name: x for x in saved_models_path.iterdir() if x.is_file()}
available_models_names = [e for e in available_models]
if verbose_available_models_names:
    print("AVAILABLE MODELS NAMES")
    print(*available_models_names, sep="\n")
    print()

print(available_models['Experimental_l2_n[20,4]_a[li,so]_bT'])
reader = ModelReader(saved_models_path.joinpath('Experimental_l2_n[20,4]_a[li,so]_bT'))
model = reader.read_pickle_model()
print(model)
