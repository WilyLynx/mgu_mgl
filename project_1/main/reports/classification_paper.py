import pathlib

import numpy as np

import main.reports.data.classification_data as cd
from main.model.functions.activation_functions.linear import linear
from main.model.functions.activation_functions.relu import relu
from main.model.functions.activation_functions.sigmoid import sigmoid
from main.model.functions.activation_functions.softmax import softmax
from main.model.functions.classification_loss.categorical_crossentropy import categorical_crossentropy
from main.model.functions.classification_loss.focal_loss import focal_loss
from main.model.optimizer import optimizer
from main.reports.utils.analyzer import Analyzer
from main.reports.utils.model_reader import ModelReader
from main.reports.utils.models_factory import ModelsFactory

# ========================================== CONFIG ==========================================
# data loading
parent_folder = "DL"  # parent of main project directory (there is "projekt1" directory with data)
data_set_number = 6  # number of selected data set (from available)
test_set_number = 2    # number of selected test data set (from available)

# models saving
save_folder_path = pathlib.Path("./ready_architectures/saved_models_architectures").resolve()

# verbose
verbose_possible_files = True
verbose_chosen_file = True
verbose_training = True

# plots
region_plots = True
loss_plots = True

# training parameters
input_batch_size = 1  # size of batch
loss_function_ = focal_loss  # loss function
optimizer_ = optimizer()  # optimizer used in training models
eta_ = 0.05  # eta for deep models trainings
epochs = 50  # number of epochs in training
validation_step = 5  # how often validation is done ( unit: epochs )

# architectures:
input_neurons = 2  # size of input
output_neurons = 4  # size of output (for regression) or different class count (classification)

# 0 hidden layers
zero_layers_analyze = False
zero_layers_save_models = True
zero_layers_neurons = []
zero_layers_activations = []
zero_layers_output_activation = softmax()
zero_layers_bias = True
zero_layers_model_name_prefix = "Zero"

# 1 hidden layer
one_layer_analyze = False
one_layer_save_models = True
one_layer_neurons = [[20, 30]]
one_layer_activations = [[linear(), relu()]]
one_layer_output_activation = softmax()
one_layer_bias = True
one_layer_model_name_prefix = "One"

# 2 hidden layers
two_layers_analyze = True
two_layers_save_models = True
two_layers_neurons = [[30], [30]]
two_layers_activations = [[relu()], [relu()]]
two_layers_output_activation = softmax()
two_layers_bias = True
two_layers_model_name_prefix = "Two"

# 3 hidden layers
three_layers_analyze = True
three_layers_save_models = True
three_layers_neurons = [[100], [50], [10]]
three_layers_activations = [[relu()], [relu()], [relu()]]
three_layers_output_activation = softmax()
three_layers_bias = True
three_layers_model_name_prefix = "Three"

# 4 hidden layers
four_layers_analyze = True
four_layers_save_models = True
four_layers_neurons = [[100, 10], [20], [10], [5]]
four_layers_activations = [[relu()], [relu()], [sigmoid(), relu()], [sigmoid()]]
four_layers_output_activation = softmax()
four_layers_bias = True
four_layers_model_name_prefix = "Four"

# other
line_length = 150
random_seed = 1234
line = line_length * '*'
np.random.seed(random_seed)

# ===================================== LOADING DATA SETS =======================================================
dl = cd.Classification_data_loader(parent_folder=parent_folder)

# possible data sets
possible_class_data_sets = dl.list_possible_data_sets()
if verbose_possible_files:
    print("POSSIBLE DATA SETS NAMES:")
    for i in range(len(possible_class_data_sets)):
        print("[{}] {}".format(i, possible_class_data_sets[i]))
    print(line)

# chosen data set
chosen_filename = possible_class_data_sets[data_set_number]
if verbose_chosen_file:
    print("CHOSEN DATA SET:")
    print("[{}] {}".format(data_set_number, chosen_filename))
    print(line)
classification_data_set = dl.get_data_set(chosen_filename)
(train_data, train_labels), (validation_data, validation_labels) = classification_data_set

# chosen test data set
chosen_filename = possible_class_data_sets[test_set_number]
if verbose_chosen_file:
    print("CHOSEN TEST DATA SET:")
    print("[{}] {}".format(test_set_number, chosen_filename))
    print(line)
test_data_set = dl.load_test_data(chosen_filename, 2)
(data, labels) = test_data_set


# ======================================= ARCHITECTURES ========================================================
# zero hidden layers
if zero_layers_analyze:
    zero_models_factory = ModelsFactory(input_neurons=input_neurons,
                                        output_neurons=output_neurons,
                                        output_activation=zero_layers_output_activation,
                                        bias=zero_layers_bias,
                                        hidden_layers_count=0,
                                        name_prefix=zero_layers_model_name_prefix)

    zero_layers_models = zero_models_factory.generate_models(neurons_set=zero_layers_neurons,
                                                             activations_set=zero_layers_activations)

    zero_model_analyzer = Analyzer(zero_layers_models)
    zero_model_analyzer.analyze_models(data=classification_data_set,
                                       test_data=test_data_set,
                                       data_loader=dl,
                                       fit_parameters={'epochs': epochs, 'eta': eta_, 'loss_function': loss_function_},
                                       validation_parameters={'validation_step': validation_step},
                                       data_name=chosen_filename,
                                       save_models=zero_layers_save_models,
                                       save_pathname=save_folder_path,
                                       verbose=verbose_training)
    if region_plots:
        zero_model_analyzer.draw_classification_regions()
    if loss_plots:
        zero_model_analyzer.draw_loss_plot()

# one hidden layer models
if one_layer_analyze:
    one_models_factory = ModelsFactory(input_neurons=input_neurons,
                                       output_neurons=output_neurons,
                                       output_activation=one_layer_output_activation,
                                       bias=one_layer_bias,
                                       hidden_layers_count=1,
                                       name_prefix=one_layer_model_name_prefix)

    one_layers_models = one_models_factory.generate_models(neurons_set=one_layer_neurons,
                                                           activations_set=one_layer_activations)

    one_model_analyzer = Analyzer(one_layers_models)
    one_model_analyzer.analyze_models(data=classification_data_set,
                                      test_data=test_data_set,
                                      data_loader=dl,
                                      fit_parameters={'epochs': epochs, 'eta': eta_, 'loss_function': loss_function_},
                                      validation_parameters={'validation_step': validation_step},
                                      data_name=chosen_filename,
                                      save_models=one_layer_save_models,
                                      save_pathname=save_folder_path,
                                      verbose=verbose_training)
    if region_plots:
        one_model_analyzer.draw_classification_regions()
    if loss_plots:
        one_model_analyzer.draw_loss_plot()

# two hidden layers
if two_layers_analyze:
    two_models_factory = ModelsFactory(input_neurons=input_neurons,
                                       output_neurons=output_neurons,
                                       output_activation=two_layers_output_activation,
                                       bias=two_layers_bias,
                                       hidden_layers_count=2,
                                       name_prefix=two_layers_model_name_prefix)

    two_layers_models = two_models_factory.generate_models(neurons_set=two_layers_neurons,
                                                           activations_set=two_layers_activations)

    two_model_analyzer = Analyzer(two_layers_models)
    two_model_analyzer.analyze_models(data=classification_data_set,
                                      test_data=test_data_set,
                                      data_loader=dl,
                                      fit_parameters={'epochs': epochs, 'eta': eta_, 'loss_function': loss_function_},
                                      validation_parameters={'validation_step': validation_step},
                                      data_name=chosen_filename,
                                      save_models=two_layers_save_models,
                                      save_pathname=save_folder_path,
                                      verbose=verbose_training)
    if region_plots:
        two_model_analyzer.draw_classification_regions()
    if loss_plots:
        two_model_analyzer.draw_loss_plot()

# three hidden layers
if three_layers_analyze:
    three_models_factory = ModelsFactory(input_neurons=input_neurons,
                                         output_neurons=output_neurons,
                                         output_activation=three_layers_output_activation,
                                         bias=three_layers_bias,
                                         hidden_layers_count=3,
                                         name_prefix=three_layers_model_name_prefix)

    three_layers_models = three_models_factory.generate_models(neurons_set=three_layers_neurons,
                                                               activations_set=three_layers_activations)

    three_model_analyzer = Analyzer(three_layers_models)
    three_model_analyzer.analyze_models(data=classification_data_set,
                                        test_data=test_data_set,
                                        data_loader=dl,
                                        fit_parameters={'epochs': epochs, 'eta': eta_, 'loss_function': loss_function_},
                                        validation_parameters={'validation_step': validation_step},
                                        data_name=chosen_filename,
                                        save_models=three_layers_save_models,
                                        save_pathname=save_folder_path,
                                        verbose=verbose_training)
    if region_plots:
        three_model_analyzer.draw_classification_regions()
    if loss_plots:
        three_model_analyzer.draw_loss_plot()

# four hidden layers
if four_layers_analyze:
    four_models_factory = ModelsFactory(input_neurons=input_neurons,
                                        output_neurons=output_neurons,
                                        output_activation=four_layers_output_activation,
                                        bias=four_layers_bias,
                                        hidden_layers_count=4,
                                        name_prefix=four_layers_model_name_prefix)

    four_layers_models = four_models_factory.generate_models(neurons_set=four_layers_neurons,
                                                             activations_set=four_layers_activations)

    four_model_analyzer = Analyzer(four_layers_models)
    four_model_analyzer.analyze_models(data=classification_data_set,
                                       test_data=test_data_set,
                                       data_loader=dl,
                                       fit_parameters={'epochs': epochs, 'eta': eta_, 'loss_function': loss_function_},
                                       validation_parameters={'validation_step': validation_step},
                                       data_name=chosen_filename,
                                       save_models=four_layers_save_models,
                                       save_pathname=save_folder_path,
                                       verbose=verbose_training)
    if region_plots:
        four_model_analyzer.draw_classification_regions()
    if loss_plots:
        four_model_analyzer.draw_loss_plot()

# print(zero_model_analyzer.statistics.to_string())
# print(zero_model_analyzer.statistics.describe().to_string())
# zero_model_analyzer.draw_classification_regions()
#
# reader = ModelReader("One hidden layers model 0")
# model_snap = reader.read_pickle_model()
# print(model_snap)

# reader = ModelReader(save_folder_path.joinpath("Four_l5_n[100,20,10,5,4]_a[re,re,re,si,so]_bT"))
# model_snap = reader.read_pickle_model()
# print(model_snap)

