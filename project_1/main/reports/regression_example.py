import numpy as np

import main.reports.data.regression_data as rd
from main.model.functions.activation_functions.leaky_relu import leaky_relu
from main.model.functions.activation_functions.linear import linear
from main.model.functions.activation_functions.relu import relu
from main.model.functions.activation_functions.sigmoid import sigmoid
from main.model.functions.activation_functions.swish import swish
from main.model.functions.activation_functions.tanh import tanh
from main.model.functions.regression_loss.mean_square import mean_square
from main.model.layers.dense import dense
from main.model.model import Model
from main.model.optimizer import optimizer
from main.reports.utils.analyzer import Analyzer


# set seed
line = 150*'*'
np.random.seed(1234)

# -------------------------------- LOADING DATA SETS --------------------------------------------
dl = rd.Regression_loader(parent_folder="DL")

print("POSSIBLE DATASETS NAMES:")
possible_class_data_sets = dl.list_possible_data_sets()
print(*possible_class_data_sets, sep="\n")
print(line)

print("CHOOSEN DATASET:")
filename = possible_class_data_sets[0]
print(filename)
print(line)
regression_data_set = dl.get_data_set(filename)
(train_data, train_labels), (validation_data, validation_labels) = regression_data_set


# ---------------------------------------------- ARCHITECTURES ---------------------------------------------------
input_neurons = 1
output_neurons = 1

hidden_layers_functions = [linear(),
                           sigmoid(),
                           leaky_relu(),
                           relu(),
                           swish(),
                           tanh()
                           ]
output_layers_functions = [linear()]

input_batch_size = 1
loss_function_ = mean_square()
optimizer_ = optimizer()
eta_ = 0.0001
epochs = 30

validation_step = 1

# 1 hidden layers

enable_bias = False
input_layers = [dense(in_len=input_neurons,
                      out_len=10,
                      activation=func,
                      enable_bias=True) for func in hidden_layers_functions]
output_layers = [dense(in_len=10,
                       out_len=output_neurons,
                       activation=output_layers_functions[0],
                       enable_bias=True) for i in range(len(input_layers))]
zero_models = [Model(layers_list=[input_layers[i],
                                  output_layers[i]],
                     name="One hidden layers model {}".format(i)) for i in range(len(input_layers))]

analyzer = Analyzer(zero_models)
analyzer.analyze_models(regression_data_set, dl,
                        fit_parameters={'epochs': epochs, 'eta': eta_})
print(analyzer.statistics.to_string())
print(analyzer.statistics.describe().to_string())
analyzer.draw_regression_plot()
