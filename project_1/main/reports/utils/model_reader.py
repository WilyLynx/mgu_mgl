import pickle


class ModelReader:
    def __init__(self, model_path):
        """
        Read pickled models
        parameters:
            model_path
        """
        self.__model_path = model_path

    def read_pickle_model(self):
        pickle_in = open(self.__model_path, "rb")
        pickled_model = pickle.load(pickle_in)
        return pickled_model
