import pathlib
import pickle

from main.reports.data.model_snapshot import ModelSnapshot
from main.reports.utils import visualiser
from main.reports.utils.visualiser import Visualiser


class model_saver:
    def __init__(self, trainer, path=pathlib.Path.cwd().joinpath("pickled_model")):
        """
        Class that save the model in chosen format
        Parameters:
            trainer - trainer that trained the model
            path - where model should be saved
        """
        self.__trainer = trainer
        self.__path = path
        (fit_parameters, validation_parameters, data_name) = trainer.get_parameters()
        visualise = Visualiser(trainer)
        self.model_snapshot = ModelSnapshot(model=trainer.model,
                                            fit_parameters=fit_parameters,
                                            validation_parameters=validation_parameters,
                                            data_name=data_name,
                                            statistics=visualise.statistics)

    def save_as_pickle(self):
        pickle_out = open(self.__path, "wb")
        pickle.dump(obj=self.model_snapshot, file=pickle_out)
        pickle_out.close()
        print("Model \"{}\" saved.".format(self.__trainer.model.name))
