import collections
from itertools import product

import numpy as np

from main.model.functions.activation_functions.sigmoid import sigmoid
from main.model.functions.activation_functions.softmax import softmax
from main.model.layers.dense import dense
from main.model.model import Model


def _seq_but_not_str(obj):
    """
    Check if obj is iterable but it is not string
    """
    return isinstance(obj, collections.abc.Iterable) and not isinstance(obj, (str, bytes, bytearray))


# noinspection PyTypeChecker
class ModelsFactory:
    def __init__(self,
                 input_neurons,
                 output_neurons,
                 output_activation=softmax(),
                 bias=True,
                 hidden_layers_count=2,
                 name_prefix=""):
        """
        Create models with exact number of hidden layers
        Parameters:
            input_neurons - size of data
            output_neurons - size of return value (or count of class when classification)
            output_activation - activation function for output layer ( default - softmax() )
            bias - if bias is enabled ( default - True )
            hidden_layers_count - default = 2
            name_prefix - name prefix for models, default = ""
        Models names are generated, but you can add your prefix to name
        """
        if hidden_layers_count < 0 or hidden_layers_count > 10:
            raise Exception("Hidden layers count should be from range [0,10]")
        self.__hidden_layers_count = hidden_layers_count
        self.name_prefix = name_prefix
        self.__input_neurons = input_neurons
        self.__output_neurons = output_neurons
        self.__bias = bias
        self.__output_activation = output_activation

    def generate_models(self,
                        neurons_set,
                        activations_set,
                        input_neurons=-1,
                        output_neurons=-1,
                        bias=-1):
        """
        Generate models from given parameters
        Parameters:
            neurons_set - list of lists - first dimension - layer no
                                        - second dimension - possibilities of neuron numbers in this layer
            activations_set - list of lists - first dimension - layer no
                                            - second dimension - possibilities of activation functions in this layer
            input_neurons
            output_neurons
            bias
        """
        self.__check_generate_models_parameter(name="Neurons set", parameter=neurons_set)
        self.__check_generate_models_parameter(name="Activations set", parameter=activations_set)
        if input_neurons == -1:
            input_neurons = self.__input_neurons
        if output_neurons == -1:
            output_neurons = self.__output_neurons
        if bias == -1:
            bias = self.__bias
        # contains possibilities of parameters for layers - list of lists
        # first dimension - No. of layer
        # second dimension - possibility number (len of this dimension could be different for every layer)
        layers_parameters = [self.__generate_layer_list(neurons_set[i],
                                                        activations_set[i]) for i in range(self.__hidden_layers_count)]
        architectures = product(*layers_parameters)
        models = []
        for architecture in architectures:
            neurons = [el[0] for el in architecture]
            activations = [el[1] for el in architecture] + [self.__output_activation]
            models = models + [self.generate_model(neurons=neurons,
                                                   activation=activations,
                                                   bias=bias,
                                                   input_neurons=input_neurons,
                                                   output_neurons=output_neurons)]
        return models

    def __check_generate_models_parameter(self, name, parameter):
        if len(parameter) != self.__hidden_layers_count:
            raise Exception("{} are set incorrectly. \n" +
                            "Should be list of lists. \n" +
                            "Length of outer list should be {}".format(name, self.__hidden_layers_count))

    def generate_model(self,
                       neurons=10,
                       activation=sigmoid(),
                       bias=-1,
                       input_neurons=-1,
                       output_neurons=-1
                       ):
        """
        Generate single model with specified parameters
        Parameters:
            neurons - number of neurons in all hidden layers or sequence of numbers
                of each hidden layers - default: 10 for all hidden layers
            activation - activation function for all hidden layers or sequence of activation
                function of each hidden layers  - default: sigmoid()
            bias - boolean for all layers (not only hidden) or sequence of bias for each layer
            output_activation_function - activation function for output layer
            """
        if input_neurons == -1:
            input_neurons = self.__input_neurons
        if output_neurons == -1:
            output_neurons = self.__output_neurons
        if bias == -1:
            bias = self.__bias
        counted_neurons = [input_neurons] + self.check_parameter(neurons, "neurons") + [output_neurons]
        counted_activation = self.check_parameter(param=activation,
                                                  param_name="activation",
                                                  should_be_size=self.__hidden_layers_count + 1)
        counted_bias = self.check_parameter(param=bias,
                                            param_name="bias",
                                            should_be_size=self.__hidden_layers_count + 1)

        layers = [dense(in_len=counted_neurons[i],
                        out_len=counted_neurons[i + 1],
                        activation=counted_activation[i],
                        enable_bias=counted_bias[i]) for i in range(len(counted_activation))]
        return Model(layers, self.__generate_model_name(layers))

    def check_parameter(self, param, param_name, should_be_size=-1):
        """
        Check if parameter is single value or sequence, if single - change do list of
        values equals to this value and length equal to should_be_size that
        is default = hidden_layers_count
        """
        if should_be_size == -1:
            should_be_size = self.__hidden_layers_count
        if not _seq_but_not_str(param):
            param = np.repeat(param, should_be_size).tolist()
        elif len(param) != should_be_size:
            raise Exception("Bad size of sequence {}. It is {} instead of {}."
                            .format(param_name, len(param), should_be_size))
        return param

    def __generate_model_name(self, layers):
        """
        Generate model name from layers specifics and model_name_prefix from client
        """
        layers_len = len(layers)
        neurons = "[" + ",".join([str(l.out_len) for l in layers]) + "]"
        activations = ",".join([str(l.activation)[:2] for l in layers])
        bias_string = str(layers[0].enable_bias)[0]
        return "{}_l{}_n{}_a[{}]_b{}".format(self.name_prefix, layers_len, neurons, activations, bias_string)

    def __generate_layer_list(self, neurons, activations):
        """
        Generate list of parameters for hidden layers (output_neurons,activation)
        from cartesian product of parameters provided in lists
        """
        if not _seq_but_not_str(neurons):
            raise Exception("Neurons for layer not in sequence")
        if not _seq_but_not_str(activations):
            raise Exception("Activations for layer not in sequence")
        layers = []
        for neuron in neurons:
            for activation in activations:
                layers.append((neuron, activation))
        return layers
