import math

import numpy as np
import copy

from main.model.functions.regression_loss.mean_square import mean_square
from main.model.optimizer import optimizer
from main.model.validatior import validator


def set_default_values_in_dictionaries(fit_parameters, validation_parameters):
    """
    Set defaults for trainer
    """
    if 'batch_size' not in fit_parameters:
        fit_parameters['batch_size'] = 1
    if 'loss_function' not in fit_parameters:
        fit_parameters['loss_function'] = mean_square
    if 'eta' not in fit_parameters:
        fit_parameters['eta'] = 0.05
    if 'optimizer' not in fit_parameters:
        fit_parameters['optimizer'] = optimizer
    if 'epochs' not in fit_parameters:
        fit_parameters['epochs'] = 30
    if 'validator' not in validation_parameters:
        validation_parameters['validator'] = validator
    if 'validation_step' not in validation_parameters:
        validation_parameters['validation_step'] = 1


class Trainer:
    def __init__(self, model, data, test_data, data_loader, fit_parameters={}, validation_parameters={}, data_name=-1,
                 mode='class'):
        """
        Parameters:
            model - deep neural network model
            fit_parameters[dictionary] - parameters of fit method:
                'batch_size' - default: 1
                'loss_function' - default: quadratic_loss()
                'eta' - default: 0.05
                'optimizer' - default: optimizer()
                'epochs' - default: 30
            validation_parameters[dictionary] - parameters needed for validation
                'validator' - default: validator()
                'validation_step' default: 1
            data - ((train_data, train_labels),(validation_data, validation_labels))
            mode - 'class' (classification) or 'reg' (regression)
        """
        # required parameters
        self.model = model
        (self.train_data, self.validation_data) = (data[0][0], data[1][0])
        (self.train_labels_raw, self.validation_labels_raw) = (data[0][1], data[1][1])
        (self.train_labels, self.validation_labels) = data_loader.get_labels(data[0][1], data[1][1])
        (self.test_data, self.test_labels) = (test_data[0], test_data[1])
        # optional parameters
        self.mode = mode
        set_default_values_in_dictionaries(fit_parameters, validation_parameters)
        self.batch_size = fit_parameters['batch_size']
        self.loss_function = fit_parameters['loss_function']()
        self.eta = fit_parameters['eta']
        self.optimizer = fit_parameters['optimizer']()
        self.epochs = fit_parameters['epochs']
        self.validator = validation_parameters['validator'](self.model)
        self.validation_step = validation_parameters['validation_step']
        # counted fields
        self.batches = self.create_batches()
        self.train_loss = []
        self.validation_loss = []
        self.best_validation_loss = 1

        self.train_error = []
        self.validation_error = []
        self.best_error = float('inf')
        self.best_model = None
        # private fields
        self.__fit_parameters = fit_parameters
        self.__validation_parameters = validation_parameters
        self.__data_name = data_name

    def create_batches(self):
        """
        Divide training_data to batches
        """
        batches_count = math.floor(len(self.train_data) / self.batch_size)
        data_batches = np.array_split(self.train_data, batches_count)
        label_batches = np.array_split(self.train_labels, batches_count)
        return [zip(data_batches[i], label_batches[i]) for i in range(batches_count)]

    def train(self, verbose=False):
        """
        Train model
        """
        if verbose:
            print("Start training {} \t".format(self.model.name))
        for epoch in range(0, self.epochs):
            if verbose:
                print("\rEpoch: {}/{}".format(epoch + 1, self.epochs), end="")
            self.batches = self.create_batches()
            for batch in self.batches:
                self.fit(batch)
            if epoch % self.validation_step == 0:
                self.validate()
        print()

    def validate(self):
        """
        Make validation on validate_data
        """
        # if self.mode == 'class':
        self.train_loss.append(self.validator.get_loss(zip(self.train_data, self.train_labels)))
        validation_loss = self.validator.get_loss(zip(self.validation_data, self.validation_labels))
        if validation_loss < self.best_validation_loss:
            self.best_validation_loss = validation_loss
            self.best_model = copy.deepcopy(self.model)
        self.validation_loss.append(validation_loss)

        if self.mode == 'reg':
            self.train_error.append(
                self.validator.get_error((self.train_data, self.train_labels), self.loss_function))
            validation_error = self.validator.get_error(
                (self.test_data, self.test_labels), self.loss_function)
            if validation_error < self.best_error:
                self.best_error = validation_error
                self.best_model = copy.deepcopy(self.model)
            self.validation_error.append(validation_error)


    def fit(self, batch):
        """
        Fit model with trainer parameters
        """
        self.model.fit(
            input_batch=batch,
            loss_function=self.loss_function,
            eta=self.eta,
            optimizer=self.optimizer
        )

    def get_parameters(self):
        """
        Return all parameters of train and validation as tuple:
        (fit_parameters, validation_parameters, data_name)
        """
        return self.__fit_parameters, self.__validation_parameters, self.__data_name
