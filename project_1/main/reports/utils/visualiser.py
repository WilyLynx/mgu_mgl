import matplotlib.pyplot as plt
import numpy as np
import matplotlib.cm as cm
from matplotlib.colors import ListedColormap
from mpl_toolkits.axes_grid1 import Grid


def acc_plot_mix(trainers, ax=False, train=True, validation=True):
    if not ax:
        ax = plt.axes(label='acc_plot_mix')
    for trainer in trainers:
        vis = Visualiser(trainer)
        vis.acc_plot(ax, train=train, validation=validation)
    ax.legend(loc=8)
    ax.set_xlabel('Epochs')
    ax.set_ylabel("Accuracy")
    ax.set_title("Accuracy")
    if not ax:
        plt.show()
    return plt.gca()


def validation_plot_mix(trainers, ax=False, train=True, validation=True):
    if not ax:
        ax = plt.axes(label='loss plot mix')
    for trainer in trainers:
        vis = Visualiser(trainer)
        vis.loss_plot(ax, train=train, validation=validation)
    ax.legend()
    ax.set_xlabel('Epochs')
    ax.set_ylabel('Loss')
    ax.set_title('Loss')
    if not ax:
        plt.show()
    return plt.gca()


def loss_plot_grid(visualizers):
    col = int(np.ceil(np.sqrt(len(visualizers))))
    row = int(np.ceil(len(visualizers) / col))
    plt.close('all')
    square_size = 4
    fig = plt.figure(figsize=(square_size*col, square_size*row))
    grid = Grid(fig, rect=111, nrows_ncols=(row, col),
                axes_pad=0.25, label_mode='L',
                )
    for ax, i in zip(grid, range(len(grid))):
        if i < len(visualizers):
            visualizers[i].train_loss_plot(ax, validation_step=visualizers[i].trainer.validation_step)
            visualizers[i].validation_loss_plot(ax, validation_step=visualizers[i].trainer.validation_step)
            ax.set_title(visualizers[i].trainer.model.name)

    plt.tight_layout()
    plt.show()


def classification_region_plot(visualizers):
    col = int(np.ceil(np.sqrt(len(visualizers))))
    row = int(np.ceil(len(visualizers) / col))
    plt.close('all')
    square_size = 4
    fig = plt.figure(figsize=(square_size*col, square_size*row))
    grid = Grid(fig, rect=111, nrows_ncols=(row, col),
                axes_pad=0.25, label_mode='L',
                )
    for ax, i in zip(grid, range(len(grid))):
        if i < len(visualizers):
            visualizers[i].classification_color_mesh_plot(ax)
            visualizers[i].data_classification_scatter_plot(ax)
            ax.set_title(visualizers[i].trainer.model.name)
    plt.tight_layout()
    plt.show()


def error_plot_grid(visualizers):
    col = int(np.ceil(np.sqrt(len(visualizers))))
    row = int(np.ceil(len(visualizers) / col))
    plt.close('all')
    square_size = 4
    fig = plt.figure(figsize=(square_size*col, square_size*row))
    grid = Grid(fig, rect=111, nrows_ncols=(row, col),
                axes_pad=0.25, label_mode='L',
                )
    for ax, i in zip(grid, range(len(grid))):
        if i < len(visualizers):
            #visualizers[i].train_error_plot(ax, validation_step=visualizers[i].trainer.validation_step)
            visualizers[i].validation_error_plot(ax, validation_step=visualizers[i].trainer.validation_step)
            ax.set_title(visualizers[i].trainer.model.name)
            ax.set_xlabel('Epochs')
            ax.set_ylabel("Error")
    plt.tight_layout()
    plt.show()


def regression_curve_plot(visualizers):
    col = int(np.ceil(np.sqrt(len(visualizers))))
    row = int(np.ceil(len(visualizers) / col))
    plt.close('all')
    square_size = 4
    fig = plt.figure(figsize=(square_size*col, square_size*row))
    grid = Grid(fig, rect=111, nrows_ncols=(row, col),
                axes_pad=0.25, label_mode='L',
                )
    for ax, i in zip(grid, range(len(grid))):
        if i < len(visualizers):
            visualizers[i].data_regression_scatter_plot(ax)
            visualizers[i].model_curve_plot(ax)
            ax.set_title(visualizers[i].trainer.model.name)
    plt.tight_layout()
    plt.show()


class Visualiser:
    """
    Visualise trainer work on neural network effects
    Parameters:
        trainer - trainer used to train neural network
    """

    def __init__(self, trainer):
        # private fields
        self.__model = trainer.model
        self.__model_name = trainer.model.name
        (self.__training_loss, self.__validation_loss) = (trainer.train_loss, trainer.validation_loss)
        (self.__training_error, self.__validation_error) = (trainer.train_error, trainer.validation_error)
        self.__training_acc = np.subtract(1, self.__training_loss)
        self.__validation_acc = np.subtract(1, self.__validation_loss)
        self.__epochs = trainer.epochs
        # public fields
        self.trainer = trainer
        self.statistics = {
            'epochs': self.__epochs,
            'last_train_loss': self.__training_loss[-1],
            'last_train_index': self.__epochs,
            'last_train_acc': self.__training_acc[-1],
            'best_train_loss': np.amin(self.__training_loss),
            'best_train_acc': np.amax(self.__training_acc),
            'best_train_indices': np.where(self.__training_loss == np.amin(self.__training_loss)),
            'last_validation_loss': self.__validation_loss[-1],
            'last_validation_acc': self.__validation_acc[-1],
            'last_validation_index': self.__epochs,
            'best_validation_loss': np.amin(self.__validation_loss),
            'best_validation_acc': np.amax(self.__validation_acc),
            'best_validation_indices': np.where(self.__validation_loss == np.amin(self.__validation_loss)),
            'epochs_to_best_result': np.min(np.where(self.__training_loss == np.amin(self.__training_loss)))
        }

    def summarise(self):
        last_train_acc = "Last train accuracy:\t\t{:0.3f}\t in epoch: {}".format(
            self.statistics['last_train_acc'],
            self.statistics['last_train_index'])
        last_validation_acc = "Last validation accuracy:\t{:0.3f}\t in epoch: {}".format(
            self.statistics['last_validation_acc'],
            self.statistics['last_validation_index'])
        best_train_acc = "Best train accuracy:\t\t{:0.3f}\t in epoch: {}".format(
            self.statistics['best_train_acc'],
            self.statistics['best_train_indices'])
        best_validation_acc = "Best validation accuracy:\t{:0.3f}\t in epoch: {}".format(
            self.statistics['best_validation_acc'],
            self.statistics['best_validation_indices'])
        print(self.__model)
        print("RESULTS AFTER {} EPOCHS".format(self.__epochs))
        print(last_train_acc)
        print(best_train_acc)
        print(last_validation_acc)
        print(best_validation_acc)

    def train_accuracy_plot(self, ax, validation_step=1, select_best=False):
        x_data = range(0, self.__epochs, validation_step)
        y_data = self.__training_acc[::validation_step]
        label = "{} - train acc".format(self.__model_name)
        label_ext = "{} - best train acc".format(self.__model_name)
        ax.plot(x_data, y_data, label=label)
        if select_best:
            ax.plot(np.min(self.statistics['best_train_indices']),
                    self.statistics['best_train_acc'],
                    'o',
                    label=label_ext)
        ax.grid(True)

    def validation_accuracy_plot(self, ax, validation_step=1, select_best=False):
        x_data = range(0, self.__epochs, validation_step)
        y_data = self.__validation_acc[::validation_step]
        label = "{} - validation acc".format(self.__model_name)
        label_ext = "{} - best validation acc".format(self.__model_name)
        ax.plot(x_data, y_data, label=label)
        if select_best:
            ax.plot(np.amin(self.statistics['best_validation_indices']),
                    self.statistics['best_validation_acc'],
                    'o',
                    label=label_ext)
        ax.grid(True)

    def train_loss_plot(self, ax, validation_step=1, select_best=False):
        x_data = range(0, self.__epochs, validation_step)
        y_data = self.__training_loss
        label = "{} - train loss".format(self.__model_name)
        label_ext = "{} - best train loss".format(self.__model_name)
        ax.plot(x_data, y_data, label=label)
        if select_best:
            ax.plot(np.amin(self.statistics['best_train_indices']),
                    self.statistics['best_train_loss'],
                    'o',
                    label=label_ext)
        ax.grid(True)

    def validation_loss_plot(self, ax, validation_step=1, select_best=False):
        x_data = range(0, self.__epochs, validation_step)
        y_data = self.__validation_loss
        label = "{} - validation loss".format(self.__model_name)
        label_ext = "{} - best validation loss".format(self.__model_name)
        ax.plot(x_data, y_data, label=label)
        if select_best:
            ax.plot(np.amin(self.statistics['best_validation_indices']),
                    self.statistics['best_validation_loss'],
                    'o',
                    label=label_ext)
        plt.grid(True)

    def train_error_plot(self, ax, validation_step=1):
        x_data = range(0, self.__epochs, validation_step)
        y_data = self.__training_error
        label = "{} - train error".format(self.__model_name)
        ax.plot(x_data, y_data, label=label)
        ax.grid(True)

    def validation_error_plot(self, ax, validation_step=1):
        x_data = range(0, self.__epochs, validation_step)
        y_data = self.__validation_error
        label = "{} - test error".format(self.__model_name)
        ax.plot(x_data, y_data, label=label)
        plt.grid(True)

    def acc_plot(self, ax, validation_step=1, select_best=False, train=True, validation=True):
        if train:
            self.train_accuracy_plot(ax, validation_step, select_best)
        if validation:
            self.validation_accuracy_plot(ax, validation_step, select_best)

    def loss_plot(self, ax, validation_step=1, select_best=False, train=True, validation=True):
        if train:
            self.train_loss_plot(ax, validation_step, select_best)
        if validation:
            self.validation_loss_plot(ax, validation_step, select_best)

    def data_classification_scatter_plot(self, ax, train=False, validation=False, test=True):
        class_count = self.trainer.model.get_out_len()
        colors_linspace = cm.rainbow(np.linspace(0, 1, class_count))
        class_colors = {cl: colour for cl, colour in zip(range(class_count), colors_linspace)}
        if train:
            colors_vector = [class_colors[cl] for cl in self.trainer.train_labels_raw]
            ax.scatter(
                x=self.trainer.train_data[::, 0],
                y=self.trainer.train_data[::, 1],
                c=colors_vector,
                marker='o',
                edgecolors='black'
            )
        if validation:
            colors_vector = [class_colors[cl] for cl in self.trainer.validation_labels_raw]
            ax.scatter(
                x=self.trainer.validation_data[::, 0],
                y=self.trainer.validation_data[::, 1],
                c=colors_vector,
                marker='v',
                edgecolors='black'
            )
        if test:
            colors_vector = [class_colors[cl] for cl in self.trainer.test_labels]
            ax.scatter(
                x=self.trainer.test_data[::, 0],
                y=self.trainer.test_data[::, 1],
                c=colors_vector,
                marker='v',
                edgecolors='black'
            )

    def classification_color_mesh_plot(self, ax, h=0.02):
        class_count = self.trainer.model.get_out_len()
        colors_linspace = cm.rainbow(np.linspace(0, 1, class_count))
        colors_light = [list(x[:3]) + [0.2] for x in colors_linspace]
        cmap_light = ListedColormap(colors_light)
        X = self.trainer.test_data[:, 0]
        Y = self.trainer.test_data[:, 1]
        x_min, x_max = X.min()-0.5, X.max()+0.5
        y_min, y_max = Y.min()-0.5, Y.max()+0.5
        xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                             np.arange(y_min, y_max, h))
        Z = self.trainer.model.forward(np.c_[xx.ravel(), yy.ravel()])
        Z = np.array([np.argmax(output) for output in Z]).reshape(xx.shape)
        ax.contourf(xx, yy, Z, cmap=cmap_light)

    def data_regression_scatter_plot(self, ax, train=False, validation=True, test=True):
        if train:
            data = np.array(sorted(list(zip(self.trainer.train_data,
                self.trainer.train_labels_raw)),
                               key= lambda x:x[0]))
            ax.plot(
                data[:, 0], data[:, 1],
                color='blue'
            )
        if validation:
            data = np.array(sorted(list(zip(self.trainer.validation_data,
                self.trainer.validation_labels_raw)),
                               key=lambda x: x[0]))
            ax.plot(
                data[:, 0], data[:, 1],
                color='orange'
            )
        if test:
            data = np.array(sorted(list(zip(self.trainer.test_data,
                self.trainer.test_labels)),
                               key=lambda x: x[0]))
            ax.plot(
                data[:, 0], data[:, 1],
                color='red'
            )

    def model_curve_plot(self, ax, h=0.02):
        X_train = self.trainer.test_data
        x_min, x_max = X_train.min()-0.5, X_train.max()+0.5
        x = [[arg] for arg in np.arange(x_min, x_max, h)]
        y = self.trainer.model.forward(x)
        flat_x = [item for sublist in x for item in sublist]
        flat_y = [item for sublist in y for item in sublist]
        ax.plot(flat_x, flat_y, color='g')










