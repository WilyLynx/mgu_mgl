import pathlib

import pandas as pd

from main.reports.utils.model_saver import model_saver
from main.reports.utils.trainer import Trainer
from main.reports.utils.visualiser import Visualiser
from main.reports.utils.visualiser import classification_region_plot
from main.reports.utils.visualiser import regression_curve_plot
from main.reports.utils.visualiser import loss_plot_grid
from main.reports.utils.visualiser import error_plot_grid


def rearrange_cols(statistics):
    cols = statistics.columns.tolist()
    cols = cols[-2:] + cols[:-2]
    statistics = statistics[cols]
    return statistics


class Analyzer:
    def __init__(self, models, mode='class'):
        self.models = models
        self.mode = mode


    def __make_trainers(self, data, test_data, data_loader, fit_parameters, validation_parameters, data_name):
        self.__trainers = [Trainer(model=x,
                                   data=data,
                                   test_data=test_data,
                                   data_name=data_name,
                                   data_loader=data_loader,
                                   fit_parameters=fit_parameters,
                                   validation_parameters=validation_parameters,
                                   mode=self.mode) for x in self.models]

    def __train(self, verbose, save_models, save_pathname):
        for trainer in self.__trainers:
            trainer.train(verbose)
            if save_models:
                saver = model_saver(trainer=trainer, path=save_pathname.joinpath(trainer.model.name))
                saver.save_as_pickle()

    def __make_visualizers(self):
        self.__visualizers = [Visualiser(trainer) for trainer in self.__trainers]

    def __count_statistics(self):
        dicts = [visualiser.statistics for visualiser in self.__visualizers]
        statistics = pd.DataFrame(dicts)
        statistics['model_name'] = [model.name for model in self.models]
        statistics['best_train_indices'] = statistics['best_train_indices'].map(lambda x: x[0][0])
        statistics['best_validation_indices'] = statistics['best_validation_indices'].map(lambda x: x[0][0])
        statistics['layers_count'] = [len(model.layers_list) for model in self.models]
        statistics = rearrange_cols(statistics)
        self.statistics = statistics

    def draw_classification_regions(self):
        classification_region_plot(self.__visualizers)

    def draw_loss_plot(self):
        loss_plot_grid(self.__visualizers)

    def draw_regression_plot(self):
        regression_curve_plot(self.__visualizers)

    def draw_error_plot(self):
        error_plot_grid(self.__visualizers)

    def analyze_models(self,
                       data,
                       test_data,
                       data_loader,
                       fit_parameters={},
                       validation_parameters={},
                       verbose=True,
                       data_name=-1,
                       save_models=False,
                       save_pathname=pathlib.Path.cwd()):
        """
        Make analyze on models
        Parameters:
            data - data to train models
            fit_parameters - dictionary of parameters used to train models:
                'batch_size' - default: 1
                'loss_function' - defaul: quadratic_loss()
                'eta' - default: 0.05
                'optimizer' - default: optimizer()
                'epochs' - default: 30
            validation_parameters - dictionary of parameters used to validate models
                'validator' - default: validator()
                'validation_step' - default: 1
            verbose - boolean - verbose mode of training models
        """
        self.__make_trainers(data=data,
                             test_data=test_data,
                             data_loader=data_loader,
                             fit_parameters=fit_parameters,
                             validation_parameters=validation_parameters,
                             data_name=data_name)
        self.__train(verbose=verbose, save_models=save_models, save_pathname=save_pathname)
        self.__make_visualizers()
        self.__count_statistics()
