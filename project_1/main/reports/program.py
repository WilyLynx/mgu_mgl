import numpy as np
from main.model.model import Model
from main.model.layers.dense import dense
from main.model.functions.activation_functions.sigmoid import sigmoid
from main.model.functions.activation_functions.softmax import softmax
from main.model.functions.regression_loss.mean_square import mean_square
from main.model.optimizer import optimizer
from main.reports.data import abstract_data_loader as dl
from main.model.validatior import validator
import matplotlib.pyplot as plt

# set seed
np.random.seed(123)


data_path = '../../../projekt1/classification/data.three_gauss.train.100.csv'
(train_data, train_labels), (validation_data, validation_labels) = dl.load_data(data_path, 2, split_ratio=0.8,
                                                                                shuffle=False)

# convert labels to vectors for classification
train_labels, validation_labels = dl.get_classification_labels(train_labels, validation_labels)

# generate model
model = Model([
    dense(in_len=2, out_len=3, activation=sigmoid(), enable_bias=False),
    dense(in_len=3, out_len=3, activation=softmax(), enable_bias=False)
])
model_validator = validator(model)


print("---------TRAINING START-----------")

batch_size = 1
epoch = 20
loss = mean_square()
SGD = optimizer()
validation_step = 1
train_acc = []
validation_acc = []
loss_val = []
for e in range(epoch):
    for i in range(int((len(train_data)/batch_size))):
        start = i*batch_size
        end = min((i+1)*batch_size, len(train_data))
        input_batch = zip(train_data[start:end, ],
                          train_labels[start:end])
        model.fit(
            input_batch,
            loss,
            eta=0.05,
            optimizer=SGD)

    if e % validation_step == 0:
        train_acc.append(model_validator.get_loss(zip(train_data, train_labels)))
        validation_acc.append(model_validator.get_loss(zip(validation_data, validation_labels)))

print("---------TRAINING END-----------")


t = np.arange(0., epoch, validation_step)
train_plot, = plt.plot(t, train_acc, 'bo-', label='Train')
validation_plot, = plt.plot(t, validation_acc, 'ro-', label='Validation')
plt.legend(handles=[train_plot, validation_plot])
plt.title('Error')
plt.xlabel('Epoch')
plt.show()


