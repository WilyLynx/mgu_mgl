import unittest

from main.model.functions.activation_functions.linear import linear
from main.model.functions.activation_functions.sigmoid import sigmoid
from main.model.functions.activation_functions.softmax import softmax
from main.model.functions.activation_functions.tanh import tanh
from main.reports.utils.models_factory import ModelsFactory
import numpy as np


class MyTestCase(unittest.TestCase):
    def test_zero_layers_model(self):
        factory = ModelsFactory(0, "model")
        model = factory.generate_model(
            input_neurons=2,
            output_neurons=3,
            neurons=20,
            activation=sigmoid(),
            bias=True
        )
        model_name = model.name
        np.testing.assert_string_equal("model l:1 | n:[3] | a:[si] | b:T", model_name)

    def test_one_layer_model(self):
        factory = ModelsFactory(1, "model")
        model = factory.generate_model(
            input_neurons=10,
            output_neurons=30,
            neurons=20,
            activation=[sigmoid(), softmax()],
            bias=False
        )
        model_name = model.name
        np.testing.assert_string_equal("model l:2 | n:[20,30] | a:[si,so] | b:F", model_name)

    def test_two_layers_model(self):
        factory = ModelsFactory(hidden_layers_count=2,
                                name_prefix="model",
                                input_neurons=2,
                                output_neurons=3,
                                bias=True)
        models = factory.generate_models(neurons_set=[[10,30,50], [20,30,50]],
                                         activations_set=[[sigmoid(), linear()], [sigmoid(), tanh()]])
        p = 1

    def test_zero_layers_model(self):
        factory = ModelsFactory(hidden_layers_count=0,
                                name_prefix="model",
                                input_neurons=2,
                                output_neurons=3,
                                bias=True)
        models = factory.generate_models(neurons_set=[],
                                         activations_set=[])
        p=1

if __name__ == '__main__':
    unittest.main()
