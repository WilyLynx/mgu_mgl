import unittest
from main.model.functions.activation_functions.tanh import tanh
import numpy as np


class test_tanh(unittest.TestCase):

    def test_tanh_0(self):
        f = tanh()
        x = np.vstack([0])
        self.assertEqual(f.func(x), np.vstack([0]))
        self.assertEqual(f.d_func(x), np.vstack([1]))

    def test_tanh_vec_zeros(self):
        f = tanh()
        x = np.vstack(np.zeros(5))
        np.testing.assert_array_equal(f.func(x), np.vstack(np.zeros(5)))
        np.testing.assert_array_equal(f.d_func(x), np.diag(np.ones(5)))


if __name__ == '__main__':
    unittest.main()
