import unittest
from main.model.functions.activation_functions.sigmoid import sigmoid
import numpy as np


class sigmoid_test(unittest.TestCase):

    def test_sigmoid_0(self):
        f = sigmoid()
        x = np.vstack([0])
        self.assertEqual(f.func(x), [[0.5]])
        self.assertEqual(f.d_func(x), [[0.25]])

    def test_sigmoid_1(self):
        f = sigmoid()
        x = np.vstack([1])
        np.testing.assert_array_almost_equal(f.func(x), [[0.7310]], 3)
        np.testing.assert_array_almost_equal(f.d_func(x), [[0.1966]], 3)

    def test_sigmoid_0_vec(self):
        f = sigmoid()
        x = np.vstack(np.zeros(5))
        np.testing.assert_array_equal(f.func(x), np.vstack([0.5, 0.5, 0.5, 0.5, 0.5]))
        np.testing.assert_array_equal(f.d_func(x), np.diag([0.25, 0.25, 0.25, 0.25, 0.25]))


if __name__ == '__main__':
    unittest.main()
