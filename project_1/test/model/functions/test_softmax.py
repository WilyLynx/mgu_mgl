import unittest
from main.model.functions.activation_functions.softmax import softmax
import numpy as np


class softmax_test(unittest.TestCase):

    def test_sigmoid_1_vec(self):
        f = softmax()
        x = np.vstack(np.ones(5))
        np.testing.assert_array_equal(f.func(x), np.vstack([0.2, 0.2, 0.2, 0.2, 0.2]))
        np.testing.assert_array_almost_equal(f.d_func(x), [
            [0.16, -0.04, -0.04, -0.04, -0.04],
            [-0.04, 0.16, -0.04, -0.04, -0.04],
            [-0.04, -0.04, 0.16, -0.04, -0.04],
            [-0.04, -0.04, -0.04, 0.16, -0.04],
            [-0.04, -0.04, -0.04, -0.04, 0.16],
        ])


if __name__ == '__main__':
    unittest.main()
