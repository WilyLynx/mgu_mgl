import unittest
from main.model.functions.activation_functions.leaky_relu import leaky_relu
import numpy as np


class test_leaky_relu(unittest.TestCase):

    def test_leaky_relu_0(self):
        f = leaky_relu()
        x = np.vstack([0])
        self.assertEqual(f.func(x), np.vstack([0]))
        self.assertEqual(f.d_func(x), np.vstack([1.0]))

    def test_leaky_relu_1(self):
        f = leaky_relu()
        x = np.vstack([1])
        np.testing.assert_array_equal(f.func(x), np.vstack([1.0]))
        np.testing.assert_array_equal(f.d_func(x), np.vstack([1.0]))

    def test_leaky_relu_vec(self):
        f = leaky_relu()
        x = np.vstack([-3, -2, -1, -0.5, 1.0, 2.0, 3.0])
        np.testing.assert_array_almost_equal(f.func(x), np.vstack([-0.3, -0.2, -0.1, -0.05, 1.0, 2.0, 3.0]))
        np.testing.assert_array_almost_equal(f.d_func(x), np.diag([0.1, 0.1, 0.1, 0.1, 1.0, 1.0, 1.0]))


if __name__ == '__main__':
    unittest.main()
