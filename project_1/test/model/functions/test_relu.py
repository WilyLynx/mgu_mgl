import unittest
from main.model.functions.activation_functions.relu import relu
import numpy as np


class test_relu(unittest.TestCase):

    def test_relu_0(self):
        f = relu()
        x = np.vstack([0])
        self.assertEqual(f.func(x), np.vstack([0]))
        self.assertEqual(f.d_func(x), np.vstack([0]))

    def test_relu_1(self):
        f = relu()
        x = np.vstack([1])
        np.testing.assert_array_equal(f.func(x), np.vstack([1.0]))
        np.testing.assert_array_equal(f.d_func(x), np.vstack([1.0]))

    def test_relu_vec(self):
        f = relu()
        x = np.vstack([0, -0.5, 1.0, 2.0, 3.0])
        np.testing.assert_array_equal(f.func(x), np.vstack([0, 0, 1.0, 2.0, 3.0]))
        np.testing.assert_array_equal(f.d_func(x), np.diag([0, 0, 1.0, 1.0, 1.0]))


if __name__ == '__main__':
    unittest.main()
