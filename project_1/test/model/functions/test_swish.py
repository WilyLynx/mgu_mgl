import unittest
from main.model.functions.activation_functions.swish import swish
import numpy as np


class test_swish(unittest.TestCase):

    def test_swish_0(self):
        f = swish()
        x = np.vstack([0])
        self.assertEqual(f.func(x), [[0.0]])
        self.assertEqual(f.d_func(x), [[0.5]])

    def test_swish_1(self):
        f = swish()
        x = np.vstack([1])
        np.testing.assert_array_almost_equal(f.func(x), [[0.7310]], 3)
        np.testing.assert_array_almost_equal(f.d_func(x), [[0.1966*1 + 0.7310*1]], 3)

    def test_swish_0_vec(self):
        f = swish()
        x = np.vstack([-1, 0, 1])
        np.testing.assert_array_almost_equal(f.func(x), np.vstack([-0.2689, 0.0, 0.7310]), decimal=4)
        np.testing.assert_array_almost_equal(f.d_func(x), np.diag([
            0.1966 * (-1) + 0.2689 * 1,
            0.25*0.0 + 0.5 * 1,
            0.1966 * 1 + 0.7310 * 1
        ]), decimal=4)


if __name__ == '__main__':
    unittest.main()
