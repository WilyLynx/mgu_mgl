import unittest
import numpy as np
from main.model.model import Model
from main.model.layers.dense import dense
from main.model.functions.activation_functions.linear import linear
from main.model.functions.regression_loss.mean_square import mean_square
from main.model import optimizer


class Model_test(unittest.TestCase):

    def test_forward_ones_3x3(self):
        """
        Weights matrix is 3x3 ones, activation is linear.
        Output for each neuron should be sum of inputs.
        """
        layer = dense(in_len=3, out_len=3, activation=linear(), enable_bias=False)
        layer.set_weigths(np.ones((3, 3)))
        test_model = Model([
            layer
        ])
        data = np.vstack(np.ones(3))
        output = test_model.forward([data])

        np.testing.assert_array_equal(output[0], [[3], [3], [3]])

    def test_forward_ones_3x4(self):
        layer = dense(in_len=3, out_len=4, activation=linear(), enable_bias=False)
        layer.set_weigths(np.ones((4, 3)))
        test_model = Model([
            layer
        ])
        data = np.vstack(np.ones(3))
        output = test_model.forward([data])

        np.testing.assert_array_equal(output[0], [[3], [3], [3], [3]])

    def test_forward_ones_3x4_diff_weights(self):
        layer = dense(in_len=3, out_len=4, activation=linear(), enable_bias=False)
        layer.set_weigths(np.array(
            [[1, 1, 1],
             [2, 2, 2],
             [3, 3, 3],
             [4, 4, 4]]
        ))
        test_model = Model([
            layer
        ])
        data = np.vstack(np.ones(3))
        output = test_model.forward([data])

        np.testing.assert_array_equal(output[0], [[3], [6], [9], [12]])

    def test__correction_single_layer_sigmoid(selfs):
        expected_result = np.vstack([1, 0])  # 0 class
        layer = dense(in_len=2, out_len=2, activation=linear(), enable_bias=True)
        layer.set_weigths(np.array(
            [[0, 0],
             [0, 0]]
        ))
        test_model = Model([
            layer
        ])
        gds = optimizer.optimizer()
        zeros_stack = np.vstack([0, 0])
        data = np.vstack([1, 1])
        internal_state = [(zeros_stack, zeros_stack)]
        loss = mean_square()
        corrections = gds.calculate_corrections(
            test_model,
            data,
            internal_state,
            loss,
            expected_result
        )
        np.testing.assert_array_almost_equal(corrections[0][0], np.array([
            [-1, 0],
            [-1, 0]
        ]))
        np.testing.assert_array_almost_equal(corrections[1][0], np.vstack([-1, 0]))


    def test__correction_complex_single_layer_sigmoid_(selfs):
        expected_result = np.vstack([1, 0])  # 0 class
        layer = dense(in_len=3, out_len=2, activation=linear(), enable_bias=True)
        layer.set_weigths(np.array(
            [[0, 0],
             [0, 0],
             [0, 0]]
        ))
        test_model = Model([
            layer
        ])
        sgd = optimizer.optimizer()
        zeros_stack = np.vstack([0, 0])
        data = np.vstack([1, 1, 1])
        internal_state = [(zeros_stack, zeros_stack)]
        loss = mean_square()
        corrections = sgd.calculate_corrections(
            test_model,
            data,
            internal_state,
            loss,
            expected_result
        )
        np.testing.assert_array_almost_equal(corrections[0][0], np.array([
            [-1, 0],
            [-1, 0],
            [-1, 0]
        ]))
        np.testing.assert_array_almost_equal(corrections[1][0], np.vstack([-1, 0]))





if __name__ == '__main__':
    unittest.main()
