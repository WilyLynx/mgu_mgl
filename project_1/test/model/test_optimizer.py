from unittest import TestCase
import numpy as np
from main.model.functions.activation_functions.linear import linear
from main.model.functions.activation_functions.sigmoid import sigmoid
from main.model.functions.regression_loss.mean_square import mean_square
from main.model import optimizer


class Test_optimizer(TestCase):
    def test__get_delta_last_linear(self):
        expected_result = np.vstack([0, 2, 0])
        last_layer_state = (np.vstack([1, 1, 1]),
                            np.vstack([1, 1, 1]))  # linear activation
        activation = linear()
        loss = mean_square()
        internal_state = [last_layer_state]
        opt = optimizer.optimizer()

        delta_L = opt.get_delta_Last(expected_result, internal_state, loss, activation)

        np.testing.assert_array_almost_equal(
            np.vstack([2 / 3, -2 / 3, 2 / 3]),
            delta_L
        )

    def test__get_delta_last_sigmoid(self):
        expected_result = np.vstack([0, 2, 0])
        activation = sigmoid()
        z = np.vstack([0, 0, 0])
        a = activation.func(z)
        last_layer_state = (z, a)
        internal_state = [last_layer_state]
        loss = mean_square()
        opt = optimizer.optimizer()

        delta_L = opt.get_delta_Last(expected_result, internal_state, loss, activation)

        np.testing.assert_array_almost_equal(
            np.vstack([1 / 12, -1 / 4, 1 / 12]),
            delta_L
        )

    def test__get_delta_l_linear_diagonal_weights(self):
        next_layer_weights = np.diag([1, 2, 3])
        last_delta_l = np.vstack([1, 2, 3])
        z_l = np.vstack([1, 1, 1])  # doesnt matter with linear activation
        activation = linear()

        opt = optimizer.optimizer()

        delta_l = opt.get_delta_l(next_layer_weights, last_delta_l, z_l, activation)

        np.testing.assert_array_almost_equal(
            np.vstack([1, 4, 9]),
            delta_l
        )

    def test__get_delta_l_linear_complex_weights(self):
        next_layer_weights = np.array(
            [[0, 2, 0],
             [3, 0, 1],
             [0, 3, 0]]
        )
        last_delta_l = np.vstack([1, 2, 3])
        z_l = np.vstack([1, 1, 1])  # doesnt matter with linear activation
        activation = linear()

        opt = optimizer.optimizer()

        delta_l = opt.get_delta_l(next_layer_weights, last_delta_l, z_l, activation)

        np.testing.assert_array_almost_equal(
            np.vstack([6, 11, 2]),
            delta_l
        )

    def test__get_delta_l_sigmoid_complex_weights(self):
        next_layer_weights = np.array(
            [[0, 2, 0],
             [3, 0, 1],
             [0, 3, 0]]
        )
        last_delta_l = np.vstack([1, 2, 3])
        z_l = np.vstack([0, 0, 0])
        activation = sigmoid()

        opt = optimizer.optimizer()

        delta_l = opt.get_delta_l(next_layer_weights, last_delta_l, z_l, activation)

        np.testing.assert_array_almost_equal(
            np.vstack([6 / 4, 11 / 4, 2 / 4]),
            delta_l
        )

    def test__get_mean_correction_simple(self):
        M_ones = np.ones((3, 3))
        weights_correction = [
            [M_ones, 2*M_ones, 3*M_ones],  # sample 1, 3x layer
            [M_ones, 2*M_ones, 3*M_ones],  # sample 2, 3x layer
        ]
        b_ones = np.vstack([1, 1, 1])
        bias_correction = [
            [b_ones, 2*b_ones, 3*b_ones],  # sample 1, 3x layer
            [b_ones, 2*b_ones, 3*b_ones],  # sample 2, 3x layer
        ]
        opt = optimizer.optimizer()

        w_mean, b_mean = opt.get_sum_correction(
            weights_correction,
            bias_correction
        )
        for i in range(3):
            np.testing.assert_array_equal(2*(i+1)*M_ones, w_mean[i])
            np.testing.assert_array_equal(2*(i+1)*b_ones, b_mean[i])
