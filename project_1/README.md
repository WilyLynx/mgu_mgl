# Projekt I - perceptron wielowarstwowy

Celem pierwszego projektu było samodzielne zaimplementowanie sieci składającej się z warstw gęstych. Samodzielnie należało także przygotować optymalizator uczący sieć na podstawie maksymalnie malejącego gradientu.

Druga część projektu obejmowała przetestowanie działania sieci na danych dostarczonych przez prowadzących oraz na zbiorze MNIST.
